package com.training.practise.part2;

import com.training.person.Person;
import com.training.practise.Dragon;
import com.training.practise.Feedable;

public class InterfacesMain {
    public static void main(String[] args) {

        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person("Horace");
        horace.setName("Horace O'Brien");

        Dragon smaug = new Dragon("Smaug");
        smaug.breatheFire();

        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[1] = smaug;

        for (Feedable element : thingsWeCanFeed) {
            if (element == null) {
                System.out.println("is null");
                continue;
            } else {
                element.feed();
            }
        }

    }

}

