package com.training.practise.part2;

public class StringBusiness {

    public static void main(String[] args) {
        String filename = "example.doc";

        filename = filename.replace(".doc", ".bak");

        System.out.println(filename);

//        Scanner sc = new Scanner(System.in);
//
//        String word1 = sc.next();
//        String word2 = sc.next();
//
//        if (word1.equals(word2)) {
//            System.out.println("Equal!");
//        } else if (word1.compareTo(word2)>0){
//            System.out.println(word1);
//        } else {
//            System.out.println(word2);
//        }
//
//        String sentence = "the quick brown fox swallowed down the lazy chicken";
//        String[] ows = sentence.split("ow");
//        System.out.println(sentence.split("ow").length -1);

        String sentence = "live not on evil";
        sentence = sentence.replace(" ", "");
        char[] chars = sentence.toCharArray();
        int count = 0;
        boolean flag = true;

        for (char element : chars) {
            if (element != chars[(chars.length - 1) - count]) {
                System.out.println("no palindrome");
                flag = false;
                break;
            }
            count++;
        }
        if (flag) {
            System.out.println("palindrome");
    }
}
}
