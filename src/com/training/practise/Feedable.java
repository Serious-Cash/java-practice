package com.training.practise;

public interface Feedable {
    public void feed();
}
