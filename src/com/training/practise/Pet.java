package com.training.practise;

public abstract class Pet implements Feedable {
    private String name;
    private int numLegs;

    public Pet() {}

    public Pet(String name) {
        this.setName(name);
    }

    public Pet(String name, int numLegs) {
        this.setName(name);
        this.setNumLegs(numLegs);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public void feed() {
        System.out.println(this.name + " is fed");
        System.out.println("and they have " + numLegs + " legs");
    }
}
