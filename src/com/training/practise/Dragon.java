package com.training.practise;

public class Dragon extends Pet implements Feedable {

    public Dragon() {
    }

    public Dragon(String name) {
        super(name);
    }

    public Dragon(String name, int numLegs) {
        super(name, numLegs);
    }

    public void feed() {
        System.out.println("Feeding Dragon : " + this.getName());
    }

    public void breatheFire() {
        System.out.println("FIRE!");
    }


}
