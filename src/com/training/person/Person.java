package com.training.person;

import com.training.practise.Feedable;

public class Person implements Feedable {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }

    public void feed() {
        System.out.println("Feeding person : " + this.getName());
    }
}
